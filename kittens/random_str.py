import random
import string


def generate_random_string(length: int) -> str:
    if length <= 0:
        raise ValueError("Length must be a positive integer greater than zero")
    random_string = ""
    characters = string.ascii_letters + string.digits
    for i in range(length):
        random_string += random.choice(characters)
    return random_string
