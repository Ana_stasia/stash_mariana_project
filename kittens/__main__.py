from kittens.random_str import generate_random_string

if __name__ == "__main__":
    print(generate_random_string(10))
