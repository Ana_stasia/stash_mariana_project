# Kittens project

Python application to improve basic programming concepts, testing, and continuous integration pipelines.

## Getting started

### Prerequisites

Python 3.11.6 or newer
pip
Poetry

### Installation

Install the project dependencies using Poetry

```
poetry install
```

### Usage

To run the Kittens application, activate the Poetry shell and execute the module:

```
poetry shell
python -m kittens
```

## Running tests

Ensure the quality of your code by running tests with the following command:

```
pytest
```

## Code Formatting

Keep your code clean and readable with Black, a Python code formatter. Install and run Black in your project environment:

```
poetry add black
black .
```

### Running with Docker

1. Building Docker image
   avigate to the root directory of the project where the Dockerfile is located and run the following command to build the Docker image:

```
docker build -t myproject .
```

This command reads the Dockerfile in the current directory and builds an image `myproject` based on the instructions within the Dockerfile.

2. Running the container
   Once the image is built, run the following command to start a container from the myproject image. This container runs the main script and prints a random string to the console:

```
docker run myproject
```
