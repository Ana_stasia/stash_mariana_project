FROM python:3.11-slim

RUN pip install --no-cache-dir poetry

WORKDIR /project

COPY . /project

RUN poetry config virtualenvs.create false \
    && poetry install --no-dev


CMD ["python", "-m", "kittens.__main__"]

