import pytest
from kittens.random_str import generate_random_string


def test_generate_random_string_length():
    # Checking if the output str has expected length
    length = 20
    result = generate_random_string(length)
    assert len(result) == length


def test_generate_random_string_randomness():
    # Checking if all generated strings are different
    result1 = generate_random_string(10)
    result2 = generate_random_string(10)
    result3 = generate_random_string(10)
    result4 = generate_random_string(10)
    result5 = generate_random_string(10)
    random_set = {result1, result2, result3, result4, result5}
    assert len(random_set) == 5


def test_generate_random_string_raise_type_error_when_str():
    with pytest.raises(TypeError):
        generate_random_string("random_str")


def test_generate_random_string_raise_type_error_when_empty():
    with pytest.raises(TypeError):
        generate_random_string()


def test_generate_random_str_raise_value_error_when_negative_number():
    with pytest.raises(ValueError):
        generate_random_string(-3)
